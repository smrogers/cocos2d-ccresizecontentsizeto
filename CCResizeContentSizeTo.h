//
//  CCResizeContentSizeTo.h
//
//  Created by Steven Rogers on 03/10/13.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface CCResizeContentSizeTo : CCActionInterval <NSCopying> {
	CGSize endSize_;
	CGSize startSize_;
	CGPoint delta_;
}

+ (id)actionWithDuration:(ccTime)duration size:(CGSize)size;
- (id)initWithDuration:(ccTime)duration size:(CGSize)size;

@end
