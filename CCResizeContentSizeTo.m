//
//  CCResizeContentSizeTo.h
//
//  Created by Steven Rogers on 03/10/13.
//

#import "CCResizeContentSizeTo.h"

@implementation CCResizeContentSizeTo

+ (id)actionWithDuration:(ccTime)t size:(CGSize)s {
	return [[self alloc] initWithDuration:t size:s];
}

- (id)initWithDuration:(ccTime)t size:(CGSize)s {
	self = [super initWithDuration:t];
	if (!self) return self;
	endSize_ = s;
	return self;
}

- (id)copyWithZone:(NSZone*)zone {
	CCAction *copy = [[[self class] allocWithZone: zone] initWithDuration:[self duration] size:endSize_];
	return copy;
}

- (void) startWithTarget:(CCNode *)aTarget {
	[super startWithTarget:aTarget];
	startSize_ = [(CCNode*)target_ contentSize];
	delta_ = ccpSub(ccpFromSize(endSize_), ccpFromSize(startSize_));
}

- (void) update: (ccTime) t {
	CGSize size = CGSizeMake((startSize_.width + delta_.x * t ), (startSize_.height + delta_.y * t));
	[target_ setContentSize:size];
}

@end